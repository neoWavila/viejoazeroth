El principal atractivo de los juegos de rol es que dotan a los jugadores de libertad casi total de acción. Por ello, es imposible que las reglas expuestas en este capítulo cubran cada situación posible. Será la DM quien tenga siempre la última palabra sobre la aplicación de las reglas, rigiéndose por el sentido común y priorizando la diversión de todos los participantes. En general, cualquier norma debe ser una guía y no una instrucción que seguir a rajatabla.

\section{Resolver acciones}
\begin{figure}[H]
\includegraphics[width=4cm]{_img/2_Reglas/1Acciones}
\centering
\end{figure}

Cuando un personaje desee llevar a cabo una acción, es la DM quien decide si esta se desarrolla con éxito o no, y qué consecuencias produce. Para ello debe sopesar la explicación aportada por el jugador, las capacidades de su aventurero y las circunstancias del momento. Generalmente este juicio es suficiente, pero en ciertas situaciones la DM puede elegir apoyarse en una tirada de dados para llegar a una decisión más imparcial.

En estos casos, el jugador lanzará 1D20. Un resultado mayor que 11 suele representar un éxito, pero la DM puede elegir aumentar o disminuir la dificultad dependiendo del contexto. Usualmente, la DM te permitirá (e incluso te pedirá) que al resultado de la tirada le sumes tus puntos en una habilidad adecuada a la situación.


\begin{rpg-suggestionbox}
Como DM, pregúntate si una tirada es realmente necesaria antes de pedirla. A un guerrero se le debería dar bien las acciones físicas, un pícaro debería saber moverse por los bajos fondos y un hechicero debería ser competente en magia básica.

Además, si decides pedir una tirada, los jugadores deberían tener más probabilidades de éxito si tienen buenas ideas o usan el equipo de forma inspirada.
\end{rpg-suggestionbox}

\subsection{Tirada de Característica}
En ciertas ocasiones, ninguna de las Habilidades encaja con la acción que el jugador desea realizar. En ese caso, lanzará 1D20 y sumará su modificador en una Característica acorde a la situación. Esto se conoce como una Tirada de Característica.

Normalmente, las acciones físicas (correr, escalar, etc.) se realizan a través de una Tirada de Característica. Se usan también para resistir (parcial o completamente) los efectos de conjuros mágicos, venenos, enfermedades o trampas, tal y como vemos en la tabla siguiente.

\begin{rpg-table}
\tableheader{Características}{Posibles usos}
Fuerza & Resistir la parálisis, petrificación... \\
Destreza & Esquivar un pozo o una trampa genérica, un proyectil sorpresa... \\
Inteligencia & Resistir un sutil control mental, librarse de un encantamiento de amistad... \\
Sabiduría & Detectar una ilusión, evitar mirar un glifo... \\
Carisma & Resistir la posesión, el sueño mágico... \\
\end{rpg-table}

La dificultad a superar será establecida por la DM, teniendo en cuenta el nivel del hechicero que lanzase el conjuro, la potencia del veneno, la calidad de la trampa...

\subsection{Tirada Enfrentada}
Cuando dos personajes realizan acciones contrarias o compiten por algo (por ejemplo, cuando un aventurero intenta timar a otro), se puede decidir el resultado mediante una Tirada Enfrentada. Ambos lanzan 1D20 y suman el bono de Característica o Habilidad que más se adecue a la situación. Aunque pueden utilizar bonos diferentes, ambos deben acordar el tipo.

El jugador con el mayor resultado supera el enfrentamiento. En caso de empate, gana el personaje que haya usado un bonificador mayor. Si son iguales, se repite la tirada.

\subsection{Ventajas, desventajas}
En ciertas ocasiones que se detallarán más adelante, la DM te pedirá que realices una tirada con ventaja o desventaja. Para ello, se lanzan 2D20 y se toma el resultado más alto en caso de ventaja o el más bajo en caso de desventaja.

\begin{rpg-table}
\tableheader{Tipo de acción}{Valor a superar}
Rutinaria                                        &   Sin tirada \\
Sencilla                                         &   10+ \\
Compleja o apresurada 	                         &   13+ \\
Difícil o bajo presión                           &   15+ \\
Extraordinario                                   &   17+ \\
Legendarias                                      &   20+ \\
\end{rpg-table}

\subsection{Trasfondos}
Un jugador puede aducir que uno de sus trasfondos le ayuda a la hora de llevar a cabo una acción. Si la DM considera que tiene razón, puede permitirle añadir un bono de +2 a la tirada. En vez del bono y si el trasfondo es muy relevante, puede permitir que la tirada se haga con ventaja.

Un trasfondo también puede jugar en contra del aventurero. Si la DM piensa que un trasfondo es un obstáculo para la acción que se está llevando a cabo, puede aplicar una penalización de -2, o incluso aplicar desventaja si el obstáculo es especialmente importante, puede pedir que la tirada se haga con desventaja.
La DM puede tratar la clase del personaje como un trasfondo más a la hora de resolver ciertas acciones si considera que a la hora de realizarlas existen diferencias significativas entre una clase y el resto. Por ejemplo, un pícaro debería robar carteras mejor que el resto de tipos.

\subsection{Rasgos de carácter}
Tras ver el resultado de la tirada, si el jugador desea un resultado más alto del obtenido pero no puede aplicar ventaja ni trasfondos, puede recurrir a los Rasgos de Carácter. Deberá argumentar por qué un Rasgo concreto le ayuda a realizar mejor una acción y gastar un Punto de Carácter. Si la DM considera la justificación aceptable, podrá sumar un +2 a la tirada o repetirla.

\begin{figure}[H]
\includegraphics[width=6.8cm]{_img/2_Reglas/1AccionesCaracter}
\centering
\end{figure}

Todos los jugadores empiezan la partida con 3 puntos, pero pueden conseguir más interpretando bien a su personaje. La DM decidirá cuándo concederlos, atendiendo a acciones interesantes, divertidas, que encajen con el personaje...

Alternativamente, si el jugador utiliza la Complicación de su personaje para meterle en problemas o situaciones desventajosas, la DM puede recompensarle con un Punto de Carácter. De forma similar, un jugador puede forzar la Complicación de otros: proponerles que lleven a cabo una acción acorde a su Complicación pero que no les convenga. Si aceptan, deberá darles uno de sus Puntos de Carácter. El objetivo no debe ser nunca que los jugadores se dediquen a incordiarse mutuamente, sino crear problemas divertidos o pequeñas disputas que hagan la historia más interesante.

\begin{rpg-warnbox}
  Los Puntos de Carácter no son acumulables de una partida a otra: siempre se inicia la partida con 3. Ni más, ni menos.
\end{rpg-warnbox}

\subsection{Críticos, pifias}
Si el valor obtenido al lanzar los dados es un 20, se dice que se ha obtenido un crítico o veinte natural y es un éxito automático. Por el contrario, un resultado de 1 se conoce como pifia y es siempre un fallo. Estos resultados pueden aprovecharse para narrar situaciones fuera de la norma, que serán recordadas por su espectacularidad o por ser un fracaso absoluto.

\subsubsection{Éxito Especial}
A veces ocurre que una tirada para utilizar alguna de tus características sale especialmente bien. Para tu aventurero, esto es una muestra de su progreso durante la historia, y notará que cada vez le es más fácil usar dicha característica. Por ello, si obtienes 18 o más en una de estas tiradas (lo que se conoce como Éxito Especial), ¡anótalo! Al subir de nivel tendrás una Oportunidad de Mejora de la característica (este proceso se explica con detalle en el \cref{sec:OportunidadMejora} en la \cpageref{sec:OportunidadMejora}).

Las Oportunidades de Mejora obtenidas se usan una única vez, y siempre en la siguiente subida de nivel. Puedes obtener más de una Oportunidad para una misma característica, pero a partir de la primera que obtengas no te servirá obtener un 18 en el dado: necesitarás un crítico.
