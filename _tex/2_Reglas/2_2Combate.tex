\section{Combate}
Cada combate se divide en múltiples rondas. Dentro de la historia, una ronda representa un pequeño intervalo de tiempo (unos diez segundos) en los que cada combatiente, tanto enemigo como aliado, realiza la acción que quiera. Este proceso se realiza de forma ordenada, de forma que todos tienen un turno en el que moverse, atacar, usar objetos o incluso buscar algún tipo de ventaja para su equipo.

El orden en el que los combatientes actúan (lo que se conoce como iniciativa) se decide a través de los valores en Destreza de cada personaje, de forma que actúe primero aquel con la puntuación más alta.

\subsection{Ataque}
\begin{figure}[H]
\includegraphics[width=8cm]{_img/2_Reglas/2Combate}
\centering
\end{figure}

Si se desea realizar un ataque, se realizan dos tiradas: la primera para saber si impacta y la segunda para averiguar cuánto daño hace en tal caso.
\begin{enumerate}[label=\textbf{\arabic*.}]
  \item \textbf{Tirada de Ataque.} Se lanza 1D20 y se suma el bono de Ataque. Si el ataque es a distancia, se suma también el modificador de Destreza, mientras que para los ataques cuerpo a cuerpo se suma el modificador de Fuerza. Si el resultado es igual o mayor al valor de Defensa del objetivo, el ataque tiene éxito. En caso contrario, lo esquiva y no es necesario realizar el siguiente paso.
  \item \textbf{Tirada de Daño.} Se decide lanzando un dado que depende del arma utilizada (para saber el dado de armas genéricas, puedes revisar el \cref{cap:TablasEquipo} de la \cpageref{cap:TablasEquipo}. En algunos casos también se podrán sumar ciertos bonos obtenidos a través de talentos, acciones previas, etc. Finalmente, se resta el valor obtenido de los Puntos de Vida del objetivo.
\end{enumerate}

\begin{rpg-warnbox}
Conviene no confundir los siguientes términos:
\begin{description}
  \item [ataque:] palabra genérica para describir una agresión cualquiera (mágica o no).
  \item [Ataque:] mecánica mediante la cual un personaje agrede a otro. Nunca se usará esta palabra para referirse a ataques mágicos.
  \item [Bono de Ataque o ATQ:] bono que depende del Nivel del personaje, según la \cref{tab:AvanceNivel} de la \cpageref{tab:AvanceNivel}.
  \item [Tirada de Ataque:] resultado obtenido al lanzar 1D20 y sumar el bono de Ataque.
\end{description}
\end{rpg-warnbox}

\begin{rpg-examplebox}
\begin{center}
  \textit{Un día cualquiera en Dun Morogh} \\
\end{center}
\textbf{Paul Mafayon} Creo que para derrotar a estos Trogs deberiamos intentar un enfoque más táctico.

\textbf{Miguel Puño de Hierro} ¡Ataco al Trog con mi espada táctica!
\begin{center}
  \textit{
    — Tirada de Ataque de Miguel — \\ 17 \\
    \smallskip
    — Defensa del Trog — \\ 13 \\
    \smallskip
    — Tirada de daño de la espada táctica — \\ 4 \\
  }
\end{center}
\end{rpg-examplebox}

\subsubsection{Cargar}
Al cargar, el aventurero puede recorrer más distancia de la habitual y realizar un ataque con ventaja. Conviene notar que cualquier ataque posterior o encadenado no tendrá ventaja, y además sufrirá un penalizador de -3 a la Defensa hasta el inicio de su siguiente turno.

\subsubsection{Ataques con dos armas}
Un personaje que combata con dos armas de una mano (por ejemplo, una daga y una espada corta) puede sumar un +1 adicional a su tirada de ataque.

\subsubsection{Armas a dos manos}
Algunas armas, como la espada larga, pueden usarse tanto a una como dos manos. Si un personaje elige empuñarla con dos manos, puede sumar un bono de +1 a las tiradas de daño con dicha arma.

\subsubsection{Pifias}
Si en la tirada de ataque se obtiene una pifia, el personaje pierde el equilibrio o deja su guardia abierta. Hasta que se inicie su siguiente ronda, todos los ataques contra él se tiran con ventaja.

\subsubsection{Impactos críticos}
Cuando se obtiene un veinte natural en una tirada de ataque, se dice que se ha obtenido un impacto crítico. Además de hacer el daño obtenido en la tirada de daño, el atacante puede describir una consecuencia especial para su oponente, que deberá ser aprobada por la DM. Siempre es válido elegir como consecuencia realizar el daño máximo posible +1.

\begin{rpg-examplebox}
Aunque cualquier idea es bienvenida a la hora de narrar las consecuencias de un impacto crítico, se debe ser cuidadoso para no descompensar en exceso el combate. Algunos ejemplos de efectos válidos son:
\begin{rpg-list}
  \item La pérdida de una pieza del equipo (por ejemplo, un escudo).
  \item Dañar la armadura del enemigo de forma que su protección baje en un punto.
  \item Forzar al enemigo a bajar la guardia para que el próximo ataque que reciba se tire con ventaja.
  \item Desplazar al enemigo.
  \item Hacer que el enemigo pierda su próximo turno.
\end{rpg-list}
\end{rpg-examplebox}

\subsubsection{Golpe de gracia}
Cualquier ataque sobre un enemigo indefenso tiene éxito por defecto y aplica el daño máximo de arma.

\begin{rpg-commentbox}
  Se considera que un oponente está indefenso si está paralizado, atado, inconsciente o, de manera general, completamente a merced de un oponente.
\end{rpg-commentbox}

\subsubsection{Resistencia y vulnerabilidad}
Algunas criaturas u objetos tienen una reacción particular a ciertos tipos de Daño.
\begin{description}
  \item[Resistencia.] El Daño contra él de dicho tipo se reduce a la mitad.
  \item[Vulnerabilidad.] El Daño contra él de dicho tipo se duplica.
\end{description}

\subsection{Interceptar}
Cualquier aventurero puede interceptar un proyectil o ataque sobre otro. Puede llevar a cabo esta acción hasta una vez por ronda, siempre y cuando tenga el atacante a la vista y aún no se hayan lanzado los dados. Las Tiradas de Ataque y Daño se lanzan con normalidad, solo que con un nuevo objetivo.

\begin{rpg-commentbox}
  Se considera proyectil cualquier cuerpo arrojadizo tanto físico como mágico. Para que se pueda interceptar, deberá ser de un tamaño y velocidad esquivables (es decir, se puede interceptar una bola de fuego pero no un meteorito)
\end{rpg-commentbox}

\subsection{Crear ventajas}
\textit{Generar} o \textit{crear ventajas} consiste en realizar acciones que ayuden al aventurero o sus aliados: pasar tiempo extrae apuntando, ocultarse antes de realizar una emboscada, distraer al enemigo... Así, el personaje o el grupo realizarán con más facilidad acciones futuras y tendrán un bonificador (o, por el contrario, harán que el enemigo tenga un penalizador).

Para saber si la ventaja se crea con éxito, se hace una tirada enfrentada de las características involucradas en realizar y detener la acción deseada. La bonificación resultante queda a discreción de la DM, atendiendo a cuánto ayuda la acción realizada en cada situación.

\begin{rpg-table}
\tableheader{Tipo de ventaja}{Bonificador}
 Leve                                                   &   +1 \\
 Grande                                                 &   +2 \\
 Enorme                                                 &   Tira con ventaja \\
\end{rpg-table}

\begin{rpg-examplebox}
\begin{center}
  \textit{Múltiples ataques tácticos más tarde...} \\
\end{center}
\textbf{Miguel Puño de Hierro:} Está bien, brujo. Tal vez tenSias razón: lo haremos a tu manera.

\textbf{Paul Mafayon:} Encárgate de distraer a los trogs mientras nosotros los emboscamos.
\begin{center}
  \textit{
    — Control de Carisma de Miguel — \\ 16 \\
    \smallskip
    — Control de Inteligencia de los Trogs — \\ 11 \\
    \smallskip
    \textit{Paul y sus compañeros obtienen ventaja en una tirada de subterfugio y la utilizan para abandonar al guerrero. Nunca más se supo de Miguel Puño de Hierro...}\\
  }
\end{center}
\end{rpg-examplebox}

\subsubsection{Enfoque defensivo}
Un personaje puede dedicar su acción a prepararse para recibir ataques. Hasta el inicio de su siguiente turno, cualquier proyectil o ataque contra él se lanza con Desventaja siempre y cuando pueda ver al atacante. Si por cualquier motivo el personaje ve su movimiento limitado, pierde su enfoque en la defensa y cualquier beneficio derivado.

Deberá declarar también cómo planea defenderse (esquivando, preparándose para resistir con mayor entereza, etc.) y así se tirarán con Desventaja las tiradas enfrentadas contra él en las que use la característica que más se adecue a su descripción entre Fuerza y Destreza.

\subsubsection{Proteger}
Cuando un aventurero protege, hasta el comienzo de su siguiente turno se centra en defender a un aliado (o varios, si están lo suficientemente cerca). Durante este tiempo, cualquier proyectil o ataque contra sus protegidos tiene un penalizador de $-2$ (o desventaja, si el personaje tiene escudo).

Para poder proteger de un ataque, el aventurero tiene que poder ver al atacante. Además, si por cualquier motivo ve su movimiento limitado, pierde su enfoque en la defensa y cualquier beneficio derivado.

\subsection{Vida y muerte}
Un personaje con 0 o menos Puntos de vida cae incapacitado y no puede actuar. Si sus puntos de vida no vuelven a ser superiores a 0 en menos de 10 rondas habrá muerto.

\begin{figure}[H]
\includegraphics[width=4cm]{_img/2_Reglas/2CombateMuerte}
\centering
\end{figure}

La DM puede permitir que el jugador incapacitado haga una Tirada de Salvación para estabilizarse. Para ello, lanzará 1D20 y sumará su nivel y su bono de Constitución, y deberá superar una dificultad de 10 + el total de PV negativos. Un personaje estabilizado no recupera la consciencia, pero dispone de una hora en vez de diez rondas para volver a tener Puntos de Vida positivos y evitar así la muerte.

\subsection{Pérdida de moral}
En una situación de verdadero peligro, pocas criaturas estarían dispuestas a jugarse la vida. Por ello, tanto jugadores como enemigos pueden decidir huir si creen que no tienen posibilidades de ganar. ¡Incluso aliados de vuestros aventureros podrían hacerlo! Alternativamente, podrían elegir rendirse (especialmente si creen que se podría negociar).

Generalmente, la DM puede decidir cuándo esto ocurre aplicando el sentido común, pero en caso de duda puede realizar una tirada de 1D20 contra una dificultad establecida según el fanatismo de la criatura y la situación. En la siguiente tabla veremos algunos ejemplos.

\begin{rpg-table}
\tableheader{Situación}{Dificultad a vencer}
El enemigo todavía dispone de una ruta de escape clara y todavía se ve con posibilidades de ganar & 6+ \\
La ruta de escape podría peligrar y el enemigo no está especialmente motivado & 11+ \\
El enemigo es cobarde, cree que es su última oportunidad para utilizar la ruta de escapa, o piensa que si se rinde los jugadores serán piadosos & 16+ \\
\end{rpg-table}

\subsubsection{Huida}
Cuando un bando decide huir de una refriega, debe realizarse en primer lugar una tirada enfrentada de 1D20, a la que se suma el valor de Destreza más bajo de entre todos los integrantes del grupo. Se comparan las tiradas y se considera que el bando con el resultado más alto gana. El primer grupo en alcanzar tres victorias alcanza su objetivo, ya sea huir o capturar al contrincante.

Para obtener ventaja en alguna de estas tiradas, el grupo puede deshacerse de comida o tesoros con la esperanza de que sus persecutores se paren a recogerlos. Dependiendo del contexto, la DM decidirá si dársela o no. Si los tesoros son lo suficienemente buenos, la persecución podría llegar a detenerse por completo. Si, al contrario, en el bando que huye se encuentran personajes incapacitados o gravemente heridos, la DM podría aplicarles desventajas.

\vfill
