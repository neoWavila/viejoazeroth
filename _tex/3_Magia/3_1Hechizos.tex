En este mundo plagado de fuerzas cósmicas en lucha constante, es imposible sobrevivir como aventurero sin dominar la magia o, en su defecto, rodearte de quien lo haga. Calcinar ejércitos, manipular los elementos, levantar a los muertos, o incluso modificar el espacio-tiempo: la imaginación es el límite. Pero, ¿a qué precio?

\section{Hechizos}
Cuando un hechicero realiza un encantamiento, le da forma mediante un nombre: \textit{Bola de fuego}, \textit{Rezo de sanación}, \textit{Invisibilidad} o \textit{Maremoto}. El nombre sugiere el efecto buscado, pero este último debe concretarse con la DM.

Además, se deben acordar los Puntos de Maná necesarios para lanzarlo. Si el hechizo propuesto es muy poderoso, la DM puede requerir sacrificios extra en forma de oro, materiales, Puntos de Vida o incluso puntos de Característica. 

Por lo general, esta preparación será previa a la partida, y se pensarán unos dos hechizos por Nivel. Estos encantamientos serán aquellos que el personaje domine mejor y haya estudiado más, pero no los únicos que podrá lanzar. Los jugadores podrán proponer nuevas ideas durante las partidas y llevarlas a cabo siempre que la DM acepte. Sin embargo, por falta de práctica estos hechizos no serán tan poderosos.

\begin{rpg-suggestionbox}
Para los hechizos que el jugador improvise durante la partida, por cada Punto de Maná gastado se recomienda conceder 1D6 o 1D4 dependiendo de si hay uno o varios objetivos, tanto para curación como daño. Generalmente, un jugador no debería poder lanzar un número de D6 mayor al Nivel de su personaje.

Si el jugador pide realizar un encantamiento muy sencillo y que un hechicero de su Nivel debería dominar (por ejemplo, encender una pequeña llama), no debería ser necesario que gastase puntos de Maná.
\end{rpg-suggestionbox}

\subsection{Lanzar hechizos}

\begin{figure}[H]
\includegraphics[width=7cm]{_img/3_Magia/1Hechizos}
\centering
\end{figure}

Una vez reunidos los requerimientos necesarios para lanzar un encantamiento solo falta comprobar si el hechicero lo lleva a cabo con éxito. Si el encantamiento es sencillo para su nivel o dispone de tiempo suficiente, se da por sentado que es capaz de realizarlo. En caso contrario, deberá hacer una tirada de dados para decidirlo.

Una Tirada de Hechizo se realiza lanzando 1D20 y sumando el modificador de Sabiduría del hechicero. Si el encantamiento no se dirige a nadie en concreto, bastará con superar una dificultad establecida por la DM para realizarlo.

Al lanzar hechizos contra seres vivos que intenten resistirse, se deberá superar su Resistencia Mágica $+$ bono de Característica. Se elegirá una Característica apropiada a la situación, para lo cual se puede utilizar como referencia las descripciones del \cref{sec:Caracteristicas} en la \cpageref{sec:Caracteristicas}.

\begin{rpg-suggestionbox}
A la hora de escribir hechizos que afecten al estado o percepción del objetivo (mareos, visiones, etc.), suele ser buena idea añadir un limitante extra (por ejemplo, que dure mientras el enemigo no reciba daño) o hacer que el contrincante pueda librarse de los efectos negativos superando una Tirada de Característica de dificultad determinada.
\end{rpg-suggestionbox}

\subsection{Interrumpir hechizos}
A la hora de realizar magia, es muy común cometer errores por falta de concentración. Por ello, recibir un ataque mientras realizas un hechizo puede retrasarlo o incluso detenerlo. Esto se decide en función del tipo de hechizo que estés intentando lanzar.
\begin{rpg-list}
	\item \textbf{Instantáneos.} Como su nombre indica, se lanzan de forma instantánea y solo ocupan medio turno. Por su rapidez, no se pueden interrumpir.
	\item \textbf{Conjuros.} Son más complejos y el hechicero debe invertir un número determinado de turnos completos en prepararlos. Es decir, se lanzan siempre al final de dicho número de turnos.

	Durante la conjuración, el hechicero no puede realizar ninguna acción, excepto defenderse de ataques. Por cada ataque recibido deberá pasar un turno extra cargando. Si la Tirada de Ataque fuese un crítico, se interrumpiría la conjuración por completo.
	\item \textbf{Canalizados.} Son aquellos cuyo efecto se repite a lo largo de una cantidad determinada de turnos. Al igual que con la conjuración, el hechicero no puede realizar otras acciones, excepto defenderse de ataques. Cualquier ataque recibido interrumpe la canalización.
\end{rpg-list}

\begin{rpg-examplebox}
\textbf{DM William:} ¿Qué hacéis este turno?

\textbf{Sacerdote Boli:} Voy a lanzar \textit{Himno de la Esperanza}. Es canalizado y dura tres turnos, así que este turno y los dos siguientes curaré 2PV a aliados cercanos a mí. Daniel Marino, estás demasiado lejos. ¡Tú no!

\textbf{Mago Daniel Marino:} Vale, pues por egoísta voy a lanzar \textit{Meteoro} sobre Boli. Es un hechizo conjurado de un turno, así que al final de este turno habré invocado un meteorito para aplastarle. Además, si acierto, paro su canalización.

\textbf{Guerrero Dalin:} Ni de broma, estoy a 1PV. Te ataco y he sacado crítico: interrumpo tu conjuración y protejo a Boli.
\end{rpg-examplebox}

\subsection{Gestión del Maná}
La mayoría de clases en Viejo Azeroth disponen de algún mecanismo a través del cual recuperar Puntos de Maná. Sin embargo, los pobres diablos que no tengan uno propio (como el cazador) deberán aprender a gestionar su Maná si no quieren quedarse sin hechizos en medio del combate.

Generalmente, un descanso de seis horas permite restablecer todos los Puntos de Maná. Si el descanso se realiza en un sitio peligroso o incómodo, puede que se necesite algo más de tiempo. En cambio, si el lugar es relajante, no será preciso esperar tanto.

\begin{rpg-commentbox}
La calidad del descanso depende en gran medida de la actitud del personaje. Puede que duerma en el palacio más lujurioso del reino, pero si no confía en su huésped descansará peor que en la chabola destartalada donde se crió.
\end{rpg-commentbox}
