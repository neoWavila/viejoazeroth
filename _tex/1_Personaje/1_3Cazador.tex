\begin{figure}[H]
\flushleft
\vspace*{-\dimexpr1in+\voffset+\topmargin+\headheight+\headsep \relax}
\hspace*{-0.3in}
\hskip-\rpghmargin \relax
\includegraphics{_img/1_Personaje/3Cazador}
\end{figure}

\subsection{Cazador}
\rpgart{t}{_img/1_Personaje/3CazadorT}
\thispagestyle{customempty}
\rpgquote{El cazador es  la auténtica arma. El arco es una mera expresión de su destreza}{Kara Brisamarina}

Ciertos aventureros escuchan desde una tierna edad la llamada de lo salvaje, que les apela a abandonar la comodidad de sus hogares en favor del implacable y primitivo mundo exterior. Aquellos que logran sobrevivir se convierten en cazadores. Maestros de su entorno, son capaces de moverse como fantasmas entre los árboles y plantar trampas en el camino de sus enemigos.

\subsubsection{Información de Clase}
Los cazadores luchan contra sus enemigos desde cualquier distancia. Sus enemigos sufren el ataque continuo de las bestias que controlan mientras ellos blanden sus armas de astas o preparan el siguiente proyectil. Son también extremadamente ágiles, y controlan el campo de batalla entorpeciendo o evadiendo a sus enemigos con facilidad.

\subsubsection{Rasgos de Clase}
Todos los cazadores poseen las siguientes características.
\begin{rpg-list}
  \item \textbf{Armas disponibles.} Cualquier arma de una mano, de asta, a distancia y bastones.
  \item \textbf{Tipo de armadura.} Armadura ligera.
  \item \textbf{Dado de Aguante.} D6.
  \item \textbf{Rol en combate.} Daño.
\end{rpg-list}

\subsubsection{Especializaciones}
Aunque todos los cazadores comparten una conexión especial con la naturaleza y sus compañeros animales, sus diferentes talentos y filosofías han resultado en las siguientes especializaciones:
\begin{rpg-list}
  \item \textbf{Bestias.} Algunos de los cazadores más talentosos forman desde la cuna un profundo vínculo con las bestias salvajes. Sienten una atracción especial por las zonas más agrestes y peligrosas, cuya naturaleza primigenia e indómita les imbuye de energía.
  \item \textbf{Supervivencia.} Estos cazadores denotan una gran preferencia por las armas de corto alcance. Se guían por su instinto y sagacidad para mantenerse al acecho y liquidar a su enemigo colocando trampas en los lugares precisos. Saben que para comprender el verdadero significado de la supervivencia primero hay que familiarizarse con el cruel rostro de la muerte.
  \item \textbf{Puntería.} Son grandes francotiradores, que disparan flechas y balas con precisión letal y ponen al descubierto las debilidades de todo aquel —o aquello— que pase por delante de su mira.
\end{rpg-list}

\subsubsection{Enfoque}
A partir del Nivel 2, el cazador aprende a acceder en combate a un estado elevado de concentración. La profundidad de su trance se representa mediante los Puntos de Enfoque (PE).

Cada cazador tiene un número concreto de PE, que se determina sumando su Nivel y su modificador de Sabiduría. Antes de realizar una Tirada de Ataque, puede gastar uno de ellos para tener Ventaja en los dados o para realizar un Ataque enfocado.

Los Puntos de Enfoque se recuperan de la misma forma que los Puntos de Maná: se restaura la mitad con un Descanso Corto y todos con uno Largo. Adicionalmente, al sacar un crítico en una Tirada de Ataque se restablece 1 Punto de Enfoque.

\paragraph{Ataques Enfocados}
Los Ataques Enfocados son habilidades especiales que el cazador ha entrenado de manera específica. Es decir, son ataques con características concretas y nombre propio: \textit{Fuego rápido}, \textit{Disparo de boleadora}, \textit{Disparo presuroso} o \textit{Disparo mortal}. Dichas habilidades usan los Puntos de Enfoque como recurso.

Al igual que los hechizos, los Ataques Enfocados se crean en colaboración con la DM, acordando un efecto y un coste en Puntos de Enfoque. Se puede leer el \cref{cap:magia} para más información e inspiración, pero se debe tener en cuenta que los Ataques Enfocados son de naturaleza no mágica.

Por cada Tirada de Ataque se puede utilizar un único Ataque Enfocado. Al llegar al Nivel 2, se aprenden dos Ataques Enfocados a elección del cazador. Además, cada dos Niveles avanzados se aprende uno nuevo. En estos puntos de aprendizaje de Ataques Enfocados, se puede aprovechar también para cambiar alguno que ya se tenga por otro.

\rpgart{b}{_img/1_Personaje/3CazadorB}

\subsubsection{Compañero animal}
Si el cazador es lo suficientemente osado para exponerse ante bestias indómitas, puede llegar a encontrar un compañero animal. Este será un momento personal, en el que se acercará a un animal salvaje y de forma pacífica intentará establecer un lazo de confianza.

Para domar a la bestia, el cazador deberá acercarse lentamente a ella, sin realizar movimientos bruscos. Es decir, no podrá defenderse de sus ataques ni esquivarlos. Deberá mantener esta actitud pacífica durante 5 rondas. Al final de estas rondas, el cazador realizará un control de Carisma cuya dificultad establecerá la DM.

Si por cualquier motivo el animal pierde la atención (si es atacado o ahuyentado, por ejemplo), se reinicia el contador. Aún así, antes de iniciar el contador el cazador puede utilizar su ingenio para evitar que el animal le ataque. Por ejemplo, podría tender algún tipo de trampa que le inmobilice pero no le haga daño, o pedirle a algún mago que lance un conjuro de parálisis.

La doma es un proceso altamente peligroso, al que el cazador suele enfrentarse en soledad (ya que otros podrían interferir atacando o defendiéndose). Conviene notar también que, como el aventurero no puede protegerse de los ataques del animal, si aprecia su integridad física nunca llegará a domar bestias gigantes, exóticas ni de mayor nivel.

\begin{rpg-commentbox}
  ¿Qué se considera una bestia exótica? La respuesta a esta pregunta queda a discreción de la DM, pero por el bien del cazador confiaremos en que sabrá diferenciar sin ayuda.
\end{rpg-commentbox}

Finalmente, el cazador debe recordar que tras la doma ha ganado un compañero y no un esclavo: aunque el animal reconozca y generalmente obedezca sus peticiones, deberá ganarse su lealtad si quiere que respete órdenes más peligrosas o exigentes de lo normal. Si exige demasiado y su compañero muere, no podrá domesticar a otro hasta pasado un periodo de luto (que será más o menos largo dependiendo de la personalidad del cazador).

\paragraph{Familia animal}
Las características, ataques y habilidades del compañero varían dependiendo de la familia animal a la que pertenezca. Además, gana una habilidad especial según su especialización (\textit{ferocidad}, \textit{tenacidad}, o \textit{astucia}). Se puede consultar con más detalle en el \cref{sec:Mascotas} en la \cpageref{sec:Mascotas}

\begin{rpg-commentbox}
  En ciertos casos, un animal podría tener habilidades, características o ataques que no sean propios de su familia (por ejemplo, si es exótico).
\end{rpg-commentbox}

\paragraph{Formando una manada}
Tras su primer compañero, el cazador podrá domar una bestia adicional cada 2 niveles. Con todo, no podrá llevar más de una consigo y tendrá que dejar el resto en una posada o establo.

Si desea tener más compañeros de los que su Nivel le permite, cada vez que retire una del establo deberá realizar un control de Carisma de dificultad 13 + nº de mascotas adicionales. Si no lo supera, perderá su lealtad.

\subsubsection{Escuela de Magia}
Gracias a su gran vínculo con la naturaleza, los cazadores más experimentados y estudiosos aprenden a fluir con la magia de la madre tierra y entender los secretos de la magia arcana.

Si el jugador tiene al menos 3 puntos en Erudición o 5 en Supervivencia aprende se inicia en el uso de magia arcana y natural, lo que le permite lanzar conjuros básicos de estas escuelas.

\subsubsection{Talentos}
\thispagestyle{customempty} %Quitando la enumeracion de la pagina 4
Todos los cazadores pueden escoger dos entre los siguientes talentos:
\begin{rpg-list}
  \item \textbf{Uno con la manada}. Posee un conocimiento exhaustivo del mundo animal que le permite domar bestias gigantes y exóticas.

  Si acierta un ataque y gasta un Punto de Enfoque, su compañero dispondrá de un +2 para un Ataque inmediato (que consume su acción del turno) sobre el mismo objetivo. Alternativamente, si un proyectil o ataque físico impacta sobre su compañero, puede utilizar un Punto de Enfoque para que lo esquive.
  \item \textbf{Maestro tirador}. El cazador dispone de una habilidad especial en cada una de las siguientes armas:
    \begin{description}
      \item [Arco.] Si acierta un Ataque con él, puede realizar un segundo disparo. No es acumulable.
      \item [Ballesta.] Si obtiene un 1 en la Tirada de Daño con ella puede volver a tirar.
      \item [Paloexplosivo.] Recargarlo consume un turno menos.
    \end{description}
  \item \textbf{Salvaje}. No puede perderse en la naturaleza (salvo bajo efectos mágicos) ni le resulta complicado ningún terreno. Siempre que pueda camuflarse usando materiales ambientales (ramitas, barro, plumas, etc.) obtiene un bono de +10 en las tiradas de Subterfugio mientras esté en sigilo.

  Puede hacerse el muerto para que los enemigos le ignoren en combate. Nunca podrá confundir a criaturas robóticas, y para engañar a humanoides deberá superar un control de Carisma contra Inteligencia.
  \item \textbf{Inmediatez}. Cuando usa armas blancas, es siempre el primero en atacar. Además, tiene un turno adicional en la primera ronda de cada combate.
\end{rpg-list}

\begin{figure}[H]
\includegraphics[width=6cm]{_img/1_Personaje/3CazadorEscudo}
\centering
\end{figure}

\cleartoleftpage
