# Viejo Azeroth alpha-0.1.0

[Ver manual](https://gitlab.com/neoWavila/viejoazeroth/-/blob/master/main.pdf)

![Preview](https://gitlab.com/neoWavila/viejoazeroth/-/raw/master/_img/_src/cover.jpg)

## ¿Qué es Viejo Azeroth?

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec erat risus, ornare mattis scelerisque sit amet, venenatis vitae ante. Donec lectus orci, molestie sit amet efficitur pellentesque, ultricies sit amet ipsum. Integer eget augue in leo pellentesque feugiat. Morbi pulvinar dolor ipsum. Ut feugiat commodo iaculis. Proin molestie lacus in mi tristique commodo. Phasellus tempus arcu quis ante tristique pellentesque. Curabitur eleifend dolor turpis, vitae ornare mi mattis aliquam. Nulla sodales placerat mauris ut eleifend. Maecenas a iaculis justo. Nulla tincidunt odio id ante scelerisque, eget ultricies mi fringilla. Suspendisse neque lectus, condimentum in interdum vel, porta id justo. Etiam gravida rhoncus faucibus. 

## ¿Qué es LaTeX?

LaTeX es un lenguaje de marcado utilizado para la creación de documentos de alta calidad, especialmente en áreas como la ciencia, la tecnología, la ingeniería y las matemáticas. Con LaTeX, los usuarios pueden escribir texto con formato profesional, incluyendo ecuaciones matemáticas complejas, gráficos y tablas. LaTeX se basa en comandos y paquetes que proporcionan una gran flexibilidad y control sobre el diseño del documento final.

## ¿Por qué se ha decidido usar LaTeX para este proyecto?

Se ha decidido utilizar LaTeX para el manual porque junto con Git permite tener un control de versiones y parches para mantener el juego balanceado. De esta forma, si alguien desea recuperar elementos eliminados o modificados, puede recompilar la versión que desea. Además, LaTeX facilita la edición del manual por cualquier usuario de la forma más cómoda posible, ya que con solo instalar LaTeX y tener un editor de texto compatible puede modificar el proyecto a su voluntad.

## ¿Cómo instalar LaTeX?

LaTeX es gratuito y multiplataforma, lo que significa que se puede instalar en Windows, macOS y Linux. A continuación, se detalla cómo instalar LaTeX en cada una de estas plataformas:

### Instalación en Windows

Para instalar LaTeX en Windows, es recomendable utilizar un distribuidor de LaTeX como Tex Live (disponible en <https://tug.org/texlive/windows.html>). Este distribuidor incluye todos los paquetes necesarios para compilar documentos LaTeX.

1. Descarga el instalador de TexLive para Windows desde <https://tug.org/texlive/windows.html#install>.
2. Ejecuta el instalador y sigue las instrucciones para completar la instalación.
3. Una vez finalizada la instalación, abre un editor de texto compatible con LaTeX, y comienza a trabajar en tus documentos LaTeX.

### Instalación en Linux

Para instalar LaTeX en Linux, es recomendable utilizar un gestor de paquetes de la distribución que se esté utilizando. Por ejemplo, en distribuciones basadas en Debian (como Ubuntu), se puede instalar LaTeX mediante el siguiente comando en la terminal:

```sh
sudo apt-get install texlive-full
```

Este comando instalará todos los paquetes necesarios para compilar documentos LaTeX.

1. Abre la terminal de Linux.
2. Ejecuta el comando de instalación correspondiente a tu distribución (por ejemplo, `sudo apt-get install texlive-full` en distribuciones basadas en Debian).
3. Espera a que se complete la instalación y abre un editor de texto compatible con LaTeX, y comienza a trabajar en tus documentos LaTeX.

## ¿Cómo compilar el manual?

Para compilar el manual de Viejo Azeroth, es necesario contar con LaTeX instalado en tu sistema operativo. Una vez que tengas LaTeX instalado, puedes compilar el manual con diferentes editores de texto. A continuación, se detallan los pasos para compilar el manual utilizando Sublime Text:

### Sublime Text

Para compilar el manual utilizando Sublime Text, es necesario tener instalado el paquete LaTeXTools para Sublime Text 2 y 3. Este paquete proporciona una serie de herramientas y comandos para trabajar con LaTeX en Sublime Text.

1. Descarga el paquete LaTeXTools desde <https://latextools.readthedocs.io/en/latest/install/>.
2. Abre el archivo `main.tex` en Sublime Text.
3. Presiona `Ctrl+Shift+B` para compilar el documento.
4. Selecciona la opción `LaTeX - Basic Builder - LuaLaTeX` en el menú que aparece. Despues de la primera compilación no es necesario seleccionar la opción nuevamente y con `Ctrl+B` compilará el proyecto con la última opción que hayamos usado. 
5. Espera a que el proceso de compilación termine. El resultado final será un archivo PDF con el manual de Viejo Azeroth.

Si tienes cualquier problema con la instalación de LaTeXTools o con la compilación del documento, puedes consultar la documentación de LaTeXTools <https://latextools.readthedocs.io/en/latest/>.


## Compresión de PDF

Los archivos PDF generados por LaTeX pueden ser muy pesados, lo que dificulta su distribución y almacenamiento. Afortunadamente, hay herramientas disponibles para comprimir estos archivos y reducir su tamaño. En esta sección, se explicará cómo hacerlo en Windows y Linux.

### Windows

Para comprimir archivos PDF en Windows, puede utilizar una herramienta como [Free PDF Compressor](https://www.freepdfcompressor.com/). Esta herramienta tiene una interfaz gráfica de usuario (GUI) amigable y es muy fácil de usar. Aquí está cómo hacerlo:

1. Descargue e instale Free PDF Compressor desde el sitio web oficial.
2. Ejecute la aplicación y haga clic en "Agregar archivo" para seleccionar el archivo PDF que desea comprimir.
3. Seleccione el nivel de compresión deseado. Cuanto mayor sea el nivel de compresión, menor será el tamaño del archivo resultante, pero la calidad de la imagen también será menor.
4. Haga clic en "Comprimir archivo" para iniciar el proceso de compresión.
5. Una vez que se haya completado la compresión, se generará un nuevo archivo PDF comprimido. Puede guardar este archivo en su disco duro o compartirlo con otros usuarios.

### Linux

En Linux, puede utilizar la herramienta [Ghostscript](https://www.ghostscript.com/) para comprimir archivos PDF. Ghostscript es una herramienta de línea de comandos que se utiliza para procesar archivos PostScript y PDF. Aquí está cómo hacerlo:

1. Instale Ghostscript en su sistema. En la mayoría de las distribuciones de Linux, puede hacerlo ejecutando el siguiente comando en una terminal:

```
sudo apt-get install ghostscript
```

2. Abra una terminal y vaya al directorio donde se encuentra el archivo PDF que desea comprimir.
3. Ejecute el siguiente comando para comprimir el archivo PDF:

```
gs -sDEVICE=pdfwrite -dNEWPDF=false -dCompatibilityLevel=1.5 -dPrinted=false -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH -sOutputFile=compressed.pdf main.pdf
```

En este comando, reemplace "input.pdf" con el nombre del archivo PDF que desea comprimir y "output.pdf" con el nombre del archivo PDF comprimido resultante. El parámetro "-dPDFSETTINGS=/ebook" establece el nivel de compresión.

4. Una vez que se complete el proceso de compresión, se generará un nuevo archivo PDF comprimido en el directorio actual. Puede guardar este archivo en su disco duro o compartirlo con otros usuarios.

Para obtener más información sobre Ghostscript y sus opciones de línea de comandos, consulte la [documentación oficial de Ghostscript](https://ghostscript.com/documentation/index.html).

Nota: El archivo PDF incluido en este repositorio fue comprimido utilizando Free PDF Compressor en Windows con la opción de configuración `-dPDFSETTINGS=/Prepress`.